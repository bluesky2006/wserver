package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/alfred-zhong/wserver"
)

func main() {
	pushURL := "http://127.0.0.1:12345/status"
	contentType := "application/json"

	pm := wserver.PushMessage{
		UserID:  "jack",
		Event:   "topic1",
		Message: fmt.Sprintf("Hello in %s", time.Now().Format("2006-01-02 15:04:05.000")),
	}
	b, _ := json.Marshal(pm)

	resp, err := http.DefaultClient.Post(pushURL, contentType, bytes.NewReader(b))
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		fmt.Println(err)
	}

	fmt.Println(string(body))
	time.Sleep(time.Second)

}
