// Package wserver provides building simple websocket server with message push.
package wserver

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

const (
	serverDefaultWSPath   = "/ws"
	serverDefaultPushPath = "/push"
	serverDefaultStatus   = "/status"
)

var defaultUpgrader = &websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(*http.Request) bool {
		return true
	},
}

// Server defines parameters for running websocket server.
type Server struct {
	// Address for server to listen on
	Addr string

	// Path for websocket request, default "/ws".
	WSPath string

	// Path for push message, default "/push".
	PushPath string

	StatusPath string

	LoopInterval int

	// Upgrader is for upgrade connection to websocket connection using
	// "github.com/gorilla/websocket".
	//
	// If Upgrader is nil, default upgrader will be used. Default upgrader is
	// set ReadBufferSize and WriteBufferSize to 1024, and CheckOrigin always
	// returns true.
	Upgrader *websocket.Upgrader

	// Check token if it's valid and return userID. If token is valid, userID
	// must be returned and ok should be true. Otherwise ok should be false.
	AuthToken func(token string) (userID string, ok bool)

	// Authorize push request. Message will be sent if it returns true,
	// otherwise the request will be discarded. Default nil and push request
	// will always be accepted.
	PushAuth func(r *http.Request) bool

	StatusAuth func(r *http.Request) bool

	PushMethod func(params map[string]string) ([]byte, error)

	wh *websocketHandler
	ph *pushHandler
	sh *statusHandler
}

// ListenAndServe listens on the TCP network address and handle websocket
// request.
func (s *Server) ListenAndServe() error {
	b := &binder{
		userID2EventConnMap: make(map[string]*[]eventConn),
		connID2UserIDMap:    make(map[string]string),
		qryCh:               make(chan int, 1),
	}

	// websocket request handler
	wh := websocketHandler{
		upgrader: defaultUpgrader,
		binder:   b,
	}
	if s.Upgrader != nil {
		wh.upgrader = s.Upgrader
	}
	if s.AuthToken != nil {
		wh.calcUserIDFunc = s.AuthToken
	}
	s.wh = &wh
	http.Handle(s.WSPath, s.wh)

	// push request handler
	ph := pushHandler{
		binder: b,
	}
	if s.PushAuth != nil {
		ph.authFunc = s.PushAuth
	}
	s.ph = &ph
	http.Handle(s.PushPath, s.ph)

	// status request handler
	sh := statusHandler{
		binder: b,
	}
	if s.StatusAuth != nil {
		sh.authFunc = s.StatusAuth
	}
	s.sh = &sh
	if s.StatusPath == "" {
		s.StatusPath = serverDefaultStatus
	}
	http.Handle(s.StatusPath, s.sh)

	if s.PushMethod != nil {
		//	s.wh.PushMethod = s.PushMethod
		var interval = 20
		if s.LoopInterval != 0 {
			interval = s.LoopInterval
		}
		go s.tickCheck(b, interval)
	}

	return http.ListenAndServe(s.Addr, nil)
}

func (s *Server) tickCheck(binder *binder, interval int) {
	//ticker := time.Tick(time.Second * 20) //定义一个10秒间隔的定时器
	ticker := time.NewTicker(time.Second * time.Duration(interval))
	//for i := range ticker {
	//fmt.Println(i) //每10秒都会执行的任务
	for {
		select {
		// 每隔20秒种检测一次文件是否更新.
		case <-ticker.C:
			now := time.Now()
			fmt.Println("ticker.C----------", now.Format("2006/01/02 15:04:05"))
			conns, ok := binder.GetAllConns()
			if ok {
				fmt.Println("---conn---", len(conns))
				for _, ws := range conns {
					ws.Conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
					// 写入一个Ping消息，如果客户端不存在了则退出该函数.
					if err := ws.Conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
						fmt.Println("---client is not exist now---", ws)
						binder.Unbind(ws)
						continue
					}
					p1 := ws.body
					b, _ := s.PushMethod(p1)
					ws.Write(b)
				}
			} else {
				fmt.Println("---conn other---", len(conns))
			}
		case data := <-binder.qryCh:
			fmt.Println("qryCH:", data)
			conns, ok := binder.GetAllConns()
			if ok {
				for _, ws := range conns {
					if ws.qryFlag != 1 {
						continue
					}
					ws.Conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
					// 写入一个Ping消息，如果客户端不存在了则退出该函数.
					if err := ws.Conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
						fmt.Println("---client is not exist now---", ws)
						binder.Unbind(ws)
						continue
					}
					p1 := ws.body
					b, _ := s.PushMethod(p1)
					ws.Write(b)
				}
			}
		}
	}
}

// Push filters connections by userID and event, then write message
func (s *Server) Push(userID, event, message string) (int, error) {
	return s.ph.push(userID, event, message)
}

// Drop find connections by userID and event, then close them. The userID can't
// be empty. The event is ignored if it's empty.
func (s *Server) Drop(userID, event string) (int, error) {
	return s.wh.closeConns(userID, event)
}

// Check parameters of Server, returns error if fail.
func (s Server) check() error {
	if !checkPath(s.WSPath) {
		return fmt.Errorf("WSPath: %s not illegal", s.WSPath)
	}
	if !checkPath(s.PushPath) {
		return fmt.Errorf("PushPath: %s not illegal", s.PushPath)
	}
	if s.WSPath == s.PushPath {
		return errors.New("WSPath is equal to PushPath")
	}

	return nil
}

// NewServer creates a new Server.
func NewServer(addr string) *Server {
	return &Server{
		Addr:     addr,
		WSPath:   serverDefaultWSPath,
		PushPath: serverDefaultPushPath,
	}
}

func checkPath(path string) bool {
	if path != "" && !strings.HasPrefix(path, "/") {
		return false
	}
	return true
}
